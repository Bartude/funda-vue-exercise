# funda-vue-exercise

## Improvements
1. Fix CORS issue when trying to use fetch to get real time data
2. Use UI Components library, like:
   * @funda/ui (was having issues with default styles. Example, button background would appear as white and text is also by default white)
   * Vuetify
   * Vue-tailwind
3. Add a carousel to show more images
4. (Personal preference) Change some default eslint rules like max-len or indent size
5. Move API Key to a .env file

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
